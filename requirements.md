# Python curriculum choices

1. Using the existing macbooks would be easier/cheaper, but for multiple caveats:
2. If you don't have the admin password for ALL of them, it will be harder
3. If they are laptops, it will be harder.
4. If there is an existing local firewall, it will be harder.
5. The scope of the course will be limited
	1. It will be limited in the fact that we can't teach linux, only linux-y concepts
	2. It will be limited in the fact that python is not preinstalled, requiring before-hand setup.
6. Age of students, and pre-existing skills.
	1. I would need to have some kind of idea about what the students have done previously in order to gauge where to start.
	2. I would feel wierd teaching students older than me.
7. What subjects to cover.
	1. Linux. The most ubiquitus operating system of our time, and the entire backbone of modern civilization. On which everything runs.
	2. Python. A great starting point to learning programming.
	3. Other langauges I could teach.
		1. These would be waaaay tougher(depending on the language), but if people already know thier stuff, then this is probably a good choice
8. Interests of the students
	1. What are the students interested in. Are they interested in games, writing flashy things like websites? or writing less sexy stuff like server-side code, and running bare-bones, but technically impressive websites.
	2. If they are intrested in games, we would do python. If they are intrested in flashy websites, we would teach web programming. If they wanted to write technically impressive websites, then we would go for golang.
